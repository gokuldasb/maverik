#[macro_use]
extern crate clap;

#[macro_use]
extern crate nom;

#[macro_use]
extern crate itertools;

use std::{fs, path::Path};

mod datalog;

fn main() {
    let args = clap_app!(Maverik =>
        (version: "0.1")
        (author: "Gokul Das B <gokuldas.dev@gmail.com>")
        (about: "MAV data extraction kit")
        (@arg outdir: -o --outdir +takes_value
            "Output directory for plot files. Default: Current Dir")
        (@arg filename: +required "Datalog file name")
    ).get_matches();

    let outdir = args.value_of("outdir").unwrap_or(".");
    let outdir = Path::new(outdir);
    let outdir = fs::canonicalize(outdir).expect("Error opening directory");
    if !outdir.is_dir() {
        panic!("Given output path is not a directory");
    }
    println!("* Output directory : {}", outdir.to_str().unwrap());

    let filename = args.value_of("filename").unwrap();
    let filename = fs::canonicalize(filename).expect("Error opening file");
    println!("* Input filename   : {}", filename.to_str().unwrap());
    let rawdata = fs::read(&filename).expect("Error reading from file");
    println!("* {} bytes read from file into memory", rawdata.len(),);

    datalog::process(&rawdata[..], outdir);
}
