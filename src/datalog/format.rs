use super::{PrimFactories, Primitive};
use std::fmt;
use std::iter;
use std::ops::Deref;

pub struct Format<'a> {
    name: String,
    id: u8,
    length: u8,
    fields: Vec<(String, Box<Primitive>, char)>,
    slices: Vec<&'a [u8]>,
}

impl<'a> Format<'a> {
    pub fn new(
        pf: &PrimFactories,
        name: &str,
        id: u8,
        length: u8,
        fmt: &str,
        colnames: &str,
    ) -> Self {
        let prims = fmt.chars().map(|a| pf.create(a));
        let fnames = colnames.split(',').map(str::to_string);
        Format {
            name: String::from(name),
            id: id,
            length: length - 3,
            fields: izip!(fnames, prims, fmt.chars()).collect(),
            slices: Vec::new(),
        }
    }

    pub fn bootstrap(pf: &PrimFactories) -> Self {
        Self::new(
            pf,
            "FMT",
            0x80,
            89,
            "BBnNZ",
            "Type,Length,Name,Format,Columns",
        )
    }

    pub fn getid(&self) -> u8 {
        self.id
    }

    pub fn len(&self) -> u8 {
        self.length
    }

    pub fn add_slice(&mut self, seg: &'a [u8]) {
        self.slices.push(seg);
    }

    pub fn parse_slices(&mut self) {
        println!("* Parsing message: {}", self);
        for seg in &self.slices {
            let mut next = *seg;
            for (_, prim, _) in &mut self.fields {
                let (i, _) = prim.parse(next).unwrap();
                next = i;
            }
        }
        println!(
            "* Parsed {} messages of type {}",
            self.slices.len(),
            self.name
        );
    }

    pub fn read(&self, name: &str) -> &Primitive {
        for (n, p, _) in &self.fields {
            if n == name {
                return p.deref();
            }
        }
        panic!(format!("Unknown field name: {}", name));
    }

    pub fn getname(&self) -> String {
        self.name.clone()
    }

    pub fn stringify(&self) -> Vec<Vec<String>> {
        let mut res = Vec::new();
        for field in &self.fields {
            let vstr = field.1.stringify().unwrap_or(
                iter::repeat(String::from(""))
                    .take(self.slices.len())
                    .collect(),
            );
            res.push(vstr);
        }
        res
    }

    pub fn heads(&self) -> Vec<String> {
        let mut res = Vec::new();
        for field in &self.fields {
            res.push(field.0.clone());
        }
        res
    }
}

impl<'a> fmt::Display for Format<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut info = String::new();
        for (n, p, c) in &self.fields {
            info.push_str(&format!("\n\tField: {:10} Format: {} ({})", n, c, p));
        }
        write!(
            f,
            "{:4} ID:0x{:02X} length: {} {}",
            self.name, self.id, self.length, info
        )
    }
}
