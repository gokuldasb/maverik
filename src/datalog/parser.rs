use super::format::Format;
use super::primitives::{PrimFactories, Primitive};
use std::collections::HashMap;

pub fn parse<'a>(rawbuf: &'a [u8]) -> HashMap<u8, Format<'a>> {
    let pfs = PrimFactories::new();
    let bootfmt = Format::bootstrap(&pfs);
    let mut formatmap: HashMap<u8, Format<'a>> = HashMap::new();
    formatmap.insert(bootfmt.getid(), bootfmt);
    // -- PASS 1 !!!
    println!("* Parse pass 1");
    sliceup(rawbuf, &pfs, &mut formatmap);
    for (_, f) in &mut formatmap {
        f.parse_slices();
    }
    let mut formatmap = make_new_formatmap(&formatmap, &pfs);
    // -- PASS 2 !!!
    println!("* Parse pass 2");
    sliceup(rawbuf, &pfs, &mut formatmap);
    for (_, f) in &mut formatmap {
        f.parse_slices();
    }
    formatmap
}

fn sliceup<'a>(rawbuf: &'a [u8], pf: &PrimFactories, fmap: &mut HashMap<u8, Format<'a>>) {
    let mut synch = pf.synchdetect();
    let mut typid = pf.typedetect();
    let mut next = rawbuf;
    while next.len() > 0 {
        match synch.parse(next) {
            Ok((i, _)) => next = i,
            Err(_) => {
                println!(
                    "* Slicing terminated with {} bytes left due to missing synch",
                    next.len()
                );
                break;
            }
        }
        let id;
        match typid.parse(next) {
            Ok((i, _)) => {
                next = i;
                id = typid.typecode();
            }
            Err(_) => {
                println!(
                    "* Slicing terminated with {} bytes left due to missing type ID",
                    next.len()
                );
                break;
            }
        }
        let &mut fmt;
        match fmap.get_mut(&id) {
            Some(x) => fmt = x,
            None => {
                println!(
                    "* Slicing terminated with {} bytes left due to invalid type ID",
                    next.len()
                );
                break;
            }
        }
        match take!(next, fmt.len()) {
            Ok((i, o)) => {
                next = i;
                fmt.add_slice(o)
            }
            Err(_) => {
                println!(
                    "* Parsing terminated with {} bytes left due to slice length mismatch",
                    next.len()
                );
                break;
            }
        }
    }
}

fn make_new_formatmap<'a>(
    oldmap: &HashMap<u8, Format<'a>>,
    pf: &PrimFactories,
) -> HashMap<u8, Format<'a>> {
    let mut newmap: HashMap<u8, Format<'a>> = HashMap::new();
    let oldfmt = oldmap.get(&0x80).unwrap();
    let name = oldfmt.read("Name").access_string().unwrap();
    let typid = oldfmt.read("Type").access_u8().unwrap();
    let length = oldfmt.read("Length").access_u8().unwrap();
    let fmtstr = oldfmt.read("Format").access_string().unwrap();
    let columns = oldfmt.read("Columns").access_string().unwrap();
    for (n, i, l, f, c) in izip!(name, typid, length, fmtstr, columns) {
        let fmt = Format::new(&pf, n, *i, *l, f, c);
        newmap.insert(fmt.getid(), fmt);
    }
    newmap
}
