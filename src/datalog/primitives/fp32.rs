use super::{PFactory, Primitive};
use nom::{le_f32, IResult};
use std::fmt;

pub struct Flot32 {
    list: Vec<f32>,
}

impl Primitive for Flot32 {
    fn new() -> Self {
        Self { list: Vec::new() }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = call!(buf, le_f32)?;
        self.list.push(res.1);
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        'f'
    }

    fn stringify(&self) -> Option<Vec<String>> {
        Some(self.list.iter().map(|i| format!("{}", i)).collect())
    }

    fn access_f32(&self) -> Option<&[f32]> {
        Some(&self.list[..])
    }
}

impl fmt::Display for Flot32 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Float 32bit LE")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(Flot32::new()))
}

pub fn key() -> char {
    Flot32::fmtchar()
}
