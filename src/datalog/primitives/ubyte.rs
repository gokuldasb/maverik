use super::{PFactory, Primitive};
use nom::{le_u8, IResult};
use std::fmt;

pub struct UByte {
    list: Vec<u8>,
}

impl Primitive for UByte {
    fn new() -> Self {
        Self { list: Vec::new() }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = call!(buf, le_u8)?;
        self.list.push(res.1);
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        'B'
    }

    fn stringify(&self) -> Option<Vec<String>> {
        Some(self.list.iter().map(|i| format!("{}", i)).collect())
    }

    fn access_u8(&self) -> Option<&[u8]> {
        Some(&self.list[..])
    }
}

impl fmt::Display for UByte {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Integer Unsigned 8bit LE")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(UByte::new()))
}

pub fn key() -> char {
    UByte::fmtchar()
}
