mod fp32;
mod pc16;
mod pcx16;
mod pe32;
mod primitive;
mod str16;
mod str4;
mod str64;
mod synch;
mod typeid;
mod ubyte;
mod uh16;
mod ui32;
mod unid;

pub use self::primitive::Primitive;
use nom::{Err::Incomplete, IResult};
use std::collections::HashMap;

type PFactory = Box<Fn() -> Box<Primitive>>;

pub struct PrimFactories {
    map: HashMap<char, PFactory>,
    unid: PFactory,
    typeid: PFactory,
    synch: PFactory,
}

impl PrimFactories {
    pub fn new() -> PrimFactories {
        let mut pf = PrimFactories {
            map: HashMap::new(),
            unid: unid::factory(),
            synch: synch::factory(),
            typeid: typeid::factory(),
        };
        pf.map.insert(ubyte::key(), ubyte::factory());
        pf.map.insert(str4::key(), str4::factory());
        pf.map.insert(str16::key(), str16::factory());
        pf.map.insert(str64::key(), str64::factory());
        pf.map.insert(ui32::key(), ui32::factory());
        pf.map.insert(fp32::key(), fp32::factory());
        pf.map.insert(pc16::key(), pc16::factory());
        pf.map.insert(pcx16::key(), pcx16::factory());
        pf.map.insert(pe32::key(), pe32::factory());
        pf.map.insert(uh16::key(), uh16::factory());
        pf
    }

    pub fn create(&self, c: char) -> Box<Primitive> {
        self.map.get(&c).unwrap_or(&self.unid)()
    }

    pub fn synchdetect(&self) -> Box<Primitive> {
        (self.synch)()
    }

    pub fn typedetect(&self) -> Box<Primitive> {
        (self.typeid)()
    }
}

// Parse null end padded string
fn nullpadstr(input: &[u8], len: usize) -> IResult<&[u8], String> {
    let a = take!(input, len)?;
    let b = take_until!(a.1, "\0");
    // Take care of the case where there is no space for null padding
    let textseg = if let Err(Incomplete(_)) = b {
        a.1
    } else {
        b?.1
    };
    Ok((a.0, String::from_utf8(textseg.to_vec()).unwrap()))
}
