use super::{PFactory, Primitive};
use nom::IResult;
use std::fmt;

pub struct UnID;

impl Primitive for UnID {
    fn new() -> Self {
        Self {}
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        Ok((buf, ()))
    }

    fn fmtchar() -> char {
        '_'
    }
}

impl fmt::Display for UnID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Unidentified/unimplemented")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(UnID::new()))
}

#[allow(dead_code)]
pub fn key() -> char {
    UnID::fmtchar()
}
