use super::{PFactory, Primitive};
use nom::{le_u16, IResult};
use std::fmt;

pub struct PCX16 {
    list: Vec<f32>,
}

impl Primitive for PCX16 {
    fn new() -> Self {
        Self { list: Vec::new() }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = call!(buf, le_u16)?;
        self.list.push(res.1 as f32 / 100f32);
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        'C'
    }

    fn stringify(&self) -> Option<Vec<String>> {
        Some(self.list.iter().map(|i| format!("{}", i)).collect())
    }

    fn access_f32(&self) -> Option<&[f32]> {
        Some(&self.list[..])
    }
}

impl fmt::Display for PCX16 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Percent:2 Fixed-point 16bit Unsigned LE")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(PCX16::new()))
}

pub fn key() -> char {
    PCX16::fmtchar()
}
