use super::{PFactory, Primitive};
use nom::{le_u16, IResult};
use std::fmt;

pub struct PC16 {
    list: Vec<f32>,
}

impl Primitive for PC16 {
    fn new() -> Self {
        Self { list: Vec::new() }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = call!(buf, le_u16)?;
        self.list.push(res.1 as f32 / 100f32);
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        'c'
    }

    fn stringify(&self) -> Option<Vec<String>> {
        Some(self.list.iter().map(|i| format!("{}", i)).collect())
    }

    fn access_f32(&self) -> Option<&[f32]> {
        Some(&self.list[..])
    }
}

impl fmt::Display for PC16 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Percent Fixed-point 16bit Unsigned LE")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(PC16::new()))
}

pub fn key() -> char {
    PC16::fmtchar()
}
