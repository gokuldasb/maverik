use super::{PFactory, Primitive};
use nom::IResult;
use std::fmt;

pub struct Synch;

impl Primitive for Synch {
    fn new() -> Self {
        Synch {}
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = tag!(buf, [0xA3, 0x95])?;
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        '@'
    }

    fn access_u8(&self) -> Option<&[u8]> {
        Some(&[0xA3, 0x95])
    }
}

impl fmt::Display for Synch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Sync-code 0xA395")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(Synch::new()))
}

#[allow(dead_code)]
pub fn key() -> char {
    Synch::fmtchar()
}
