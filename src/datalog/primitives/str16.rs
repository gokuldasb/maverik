use super::nullpadstr;
use super::{PFactory, Primitive};
use nom::IResult;
use std::fmt;

pub struct Str16 {
    list: Vec<String>,
}

impl Primitive for Str16 {
    fn new() -> Self {
        Self { list: Vec::new() }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = apply!(buf, nullpadstr, 16)?;
        self.list.push(res.1);
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        'N'
    }

    fn stringify(&self) -> Option<Vec<String>> {
        Some(self.list.clone())
    }

    fn access_string(&self) -> Option<&[String]> {
        Some(&self.list[..])
    }
}

impl fmt::Display for Str16 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "String 16byte Null-padded")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(Str16::new()))
}

pub fn key() -> char {
    Str16::fmtchar()
}
