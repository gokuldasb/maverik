use super::{PFactory, Primitive};
use nom::{le_i32, IResult};
use std::fmt;

pub struct PE32 {
    list: Vec<f32>,
}

impl Primitive for PE32 {
    fn new() -> Self {
        Self { list: Vec::new() }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = call!(buf, le_i32)?;
        self.list.push(res.1 as f32 / 100f32);
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        'e'
    }

    fn stringify(&self) -> Option<Vec<String>> {
        Some(self.list.iter().map(|i| format!("{}", i)).collect())
    }

    fn access_f32(&self) -> Option<&[f32]> {
        Some(&self.list[..])
    }
}

impl fmt::Display for PE32 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Percent Fixed-point 32bit Unsigned LE")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(PE32::new()))
}

pub fn key() -> char {
    PE32::fmtchar()
}
