use nom::IResult;
use std::fmt::Display;

pub trait Primitive: Display {
    fn new() -> Self
    where
        Self: Sized;

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()>;

    fn fmtchar() -> char
    where
        Self: Sized;

    fn stringify(&self) -> Option<Vec<String>> {
        None
    }

    fn typecode(&self) -> u8 {
        0
    }

    fn access_u8(&self) -> Option<&[u8]> {
        None
    }

    fn access_u32(&self) -> Option<&[u32]> {
        None
    }

    fn access_f32(&self) -> Option<&[f32]> {
        None
    }

    fn access_u16(&self) -> Option<&[u16]> {
        None
    }

    fn access_string(&self) -> Option<&[String]> {
        None
    }
}
