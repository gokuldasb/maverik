use super::{PFactory, Primitive};
use nom::{le_u32, IResult};
use std::fmt;

pub struct UInt32 {
    list: Vec<u32>,
}

impl Primitive for UInt32 {
    fn new() -> Self {
        Self { list: Vec::new() }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = call!(buf, le_u32)?;
        self.list.push(res.1);
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        'I'
    }

    fn stringify(&self) -> Option<Vec<String>> {
        Some(self.list.iter().map(|i| format!("{}", i)).collect())
    }

    fn access_u32(&self) -> Option<&[u32]> {
        Some(&self.list[..])
    }
}

impl fmt::Display for UInt32 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Integer 32bit Unsigned LE")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(UInt32::new()))
}

pub fn key() -> char {
    UInt32::fmtchar()
}
