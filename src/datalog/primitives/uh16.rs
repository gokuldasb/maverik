use super::{PFactory, Primitive};
use nom::{le_u16, IResult};
use std::fmt;

pub struct UH16 {
    list: Vec<u16>,
}

impl Primitive for UH16 {
    fn new() -> Self {
        Self { list: Vec::new() }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = call!(buf, le_u16)?;
        self.list.push(res.1);
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        'h'
    }

    fn stringify(&self) -> Option<Vec<String>> {
        Some(self.list.iter().map(|i| format!("{}", i)).collect())
    }

    fn access_u16(&self) -> Option<&[u16]> {
        Some(&self.list[..])
    }
}

impl fmt::Display for UH16 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Integer 16bit Unsigned LE")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(UH16::new()))
}

pub fn key() -> char {
    UH16::fmtchar()
}
