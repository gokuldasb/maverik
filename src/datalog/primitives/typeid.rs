use super::{PFactory, Primitive};
use nom::{le_u8, IResult};
use std::fmt;

pub struct TypeID {
    id: u8,
}

impl Primitive for TypeID {
    fn new() -> Self {
        TypeID { id: 0 }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = call!(buf, le_u8)?;
        self.id = res.1;
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        '#'
    }

    fn typecode(&self) -> u8 {
        self.id
    }
}

impl fmt::Display for TypeID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Message Type ID")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(TypeID::new()))
}

#[allow(dead_code)]
pub fn key() -> char {
    TypeID::fmtchar()
}
