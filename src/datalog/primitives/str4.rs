use super::nullpadstr;
use super::{PFactory, Primitive};
use nom::IResult;
use std::fmt;

pub struct Str4 {
    list: Vec<String>,
}

impl Primitive for Str4 {
    fn new() -> Self {
        Self { list: Vec::new() }
    }

    fn parse<'a>(&mut self, buf: &'a [u8]) -> IResult<&'a [u8], ()> {
        let res = apply!(buf, nullpadstr, 4)?;
        self.list.push(res.1);
        Ok((res.0, ()))
    }

    fn fmtchar() -> char {
        'n'
    }

    fn stringify(&self) -> Option<Vec<String>> {
        Some(self.list.clone())
    }

    fn access_string(&self) -> Option<&[String]> {
        Some(&self.list[..])
    }
}

impl fmt::Display for Str4 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "String 4byte Null-padded")
    }
}

pub fn factory() -> PFactory {
    Box::new(|| Box::new(Str4::new()))
}

pub fn key() -> char {
    Str4::fmtchar()
}
