mod format;
mod parser;
mod primitives;

pub use self::primitives::{PrimFactories, Primitive};
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

pub fn process<'a>(rawbuf: &'a [u8], outdir: PathBuf) {
    let map = parser::parse(rawbuf);
    for msg in map.values() {
        let mut filename = outdir.clone();
        filename.push(msg.getname() + ".csv");
        println!("* Creating file: {}", filename.to_str().unwrap());
        let mut file = File::create(filename).expect("Error opening file for writing");
        write!(file, "{}\n", msg.heads().join("\t")).expect("Error writing to file");
        for line in transpose(msg.stringify()) {
            write!(file, "{}\n", line.join("\t")).expect("Error writing to file");
        }
    }
}

// Invariant: All inner vecs should be of equal length
fn transpose<T: Clone>(ip: Vec<Vec<T>>) -> Vec<Vec<T>> {
    let len = ip.iter().map(|i| i.len()).max().unwrap_or(0);
    let mut op: Vec<Vec<T>> = vec![Vec::new(); len];
    for invec in ip {
        for (item, oline) in izip!(invec, &mut op) {
            oline.push(item);
        }
    }
    op
}
